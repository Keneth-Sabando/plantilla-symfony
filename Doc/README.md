# Practica Symfony


# INFORMACION DEL PROYECTO:

## Nombre del proyecto:

[Nombre del Proyecto]

## Descripcion del proyecto:

[Descripcion del proyecto]


# Requisitos Previos
- Servidor local, APACHE
- PHP
- Composer
- Symfony CLI
- IDE o editor de Texto
- Postgres
> Nota: Para la implementacion de estilos, se implementacion Boostrap. 


# Autor ✒️
- Keneth Joel Sabando Solorzano
